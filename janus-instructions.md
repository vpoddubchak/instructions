# How to use Janus SDK
1. To build Janus server we use those steps:
  - Install dependencies:
  ```
  aptitude install libmicrohttpd-dev libjansson-dev libnice-dev \
          libssl-dev libsrtp-dev libsofia-sip-ua-dev libglib2.0-dev \
          libopus-dev libogg-dev libcurl4-openssl-dev pkg-config gengetopt \
          libtool automake
```
  - install libsrtp 2.0
  ```
  wget https://github.com/cisco/libsrtp/archive/v1.5.4.tar.gz
  tar xfv v1.5.4.tar.gz
  cd libsrtp-1.5.4
  ./configure --prefix=/usr --enable-openssl
  make shared_library && sudo make install
```
  - install usrsctp
 ```
 git clone https://github.com/sctplab/usrsctp
 cd usrsctp
 ./bootstrap
 ./configure --prefix=/usr && make && sudo make install
```
  - install libwebsockets
 ```
 git clone git://git.libwebsockets.org/libwebsockets
 cd libwebsockets
 # If you want the stable version of libwebsockets, uncomment the next line
 # git checkout v2.4-stable
 mkdir build
 cd build
 # See https://github.com/meetecho/janus-gateway/issues/732 re: LWS_MAX_SMP
 cmake -DLWS_MAX_SMP=1 -DCMAKE_INSTALL_PREFIX:PATH=/usr -DCMAKE_C_FLAGS="-fpic" ..
 make && sudo make install
```
  - install Eclipse Paho MQTT C
 ```
 git clone https://github.com/eclipse/paho.mqtt.c.git
 cd paho.mqtt.c
 make && sudo make install
```
  - get the code (or unzip archive with code)
 ```
 git clone https://bitbucket.org/mobilinq/janus-gateway.git
 cd janus-gateway
 ```
  - generate the configure file
  ```
    sh autogen.sh
```
  - configure and compile
 ```
 ./configure --prefix=/opt/janus
 make
 make install
```
  - install the default configuration files to use
 ```
  make configs
 ```
  - run Janus with --nat option to avoid ice negotiations problem on Amazon ec2. use --help to see all available commands
```
./janus --nat-1-1=<publicIP>
```

2. To allow SSL connection to Janus it is need to make changes in config files which are located in /opt/janus/etc/janus:
  - modify **janus.transport.http.cfg**:
    add pathes to cert files:
```
cert_pem = /.../cert.pem
cert_key = /.../privkey.pem
```
    uncomment parameters https and secure_port:
```
https = yes
secure_port = 8089
```
  - modify **janus.transport.websockets.cfg**:
    add pathes to cert files:
```
cert_pem = /.../cert.pem
cert_key = /.../privkey.pem
```
    uncomment parameters https and secure_port:
```
wss = yes
wss_port = 8989
```

3. To change signaling protocol from http (default) to websocket
need to change url in file **janus-server/html/siptest.js** from:
```
server = "https://" + window.location.hostname + ":8089/janus";
```
to:
```
server = "wss://" + window.location.hostname + ":8989/janus";
```
details are [there](https://janus.conf.meetecho.com/docs/deploy.html) in "Using Janus with WebSockets" section.

4. To start SIP Gateway demo, need to use some http server.
We propose to use [http-server](https://www.npmjs.com/package/http-server):
 - install http-server
 ```
 npm install http-server -g
 ```
 - run http-server with ssl
 ```
 http-server /.../janus-server/html -S -C /.../cert.pem -K /.../privkey.pem -p 443
 ```

5. After Janus and http servers are running, you can access demo from browser.
It is on index.html page. When it started, the demo will allow you to insert
a minimum set of information required to REGISTER the web page as a SIP client
at a SIP Proxy or PBX you specify. This will allow you to call SIP URIs,
or receive calls through the SIP Server itself.
During a call, you'll also be able to interact with the PBX via DTMF tones,
e.g., to drive an Interactive Voice Response (IVR) menu that you're being presented with.
Press the Start button to launch the demo.

6. To register user from demo web-client user should do the following:
 - Fill edit boxes. For example for user mA0fFrP0 on server 185.138.169.33 it will look like this:
 ![alt text](https://assets.gitlab-static.net/vpoddubchak/instructions/raw/master/sip_credentials.png "")
 - select "Register using plain secret" in Register approach menu:
 ![alt text](https://assets.gitlab-static.net/vpoddubchak/instructions/raw/master/register%20approach.png "")
 - press "Register" btn

7. To make call, put sip adress to appeared edit box and press "Call" btn.
  For example to call to sip:0732725375@185.138.169.33:
  ![alt text](https://assets.gitlab-static.net/vpoddubchak/instructions/raw/master/call.png "")

8. Web client supports putting of P-ASSERTED-IDENTITY header. Just need to fill
corresponding edit box. Web client will transfer this value to Janus server
at the moment when user press Call btn. Then janus server will send it during sip signaling.

9. Janus doc is not well structured, so here are useful links:

| Link        | Description     |
| ------------- |-------------|
| [JanusAPI](https://janus.conf.meetecho.com/docs/JS.html) | JavaScript API: how to inti and use Janus object
| [Interfaces](https://janus.conf.meetecho.com/docs/rest.html) | RESTful, WebSockets, RabbitMQ, MQTT and UnixSockets API      |
| [AdminAPI](https://janus.conf.meetecho.com/docs/admin.html) | Admin/Monitor API that can be used to ask Janus for more specific information related to sessions and handles. This is especially useful when you want to debug issues at the media level. |
| [Deploy](https://janus.conf.meetecho.com/docs/deploy.html) | Useful information for custom deploying server |

10, To configure automatic restart of Janus, need to do the following
  - Modify "ExecStart" value in files http-server.service and janus.service to have paths suitable for current server.
  - Make symlink of files http-server.service and janus.service into folder /etc/systemd/system/multi-user.target.wants
  - Make sure janus.service has correct parameters, in our case add: --nat --nat-1-1=<publicIP>
  - reboot server
